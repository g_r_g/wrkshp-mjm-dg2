$(document).ready(() => {

  /************ POP VALISE ************/
  $(document).ready(() => {
    $("#todoPop").dialog({
        autoOpen: false,
        title: "TO DO LIST ::",
        width: 500,
        height: 500
    });
    $("#valisePop").dialog({
      autoOpen: false,
      title: "BOITE À OUT'S ::",
      width: 550,
      height: 280,
    });
    $("#freeImgPop").dialog({
      autoOpen: false,
      title: "Images libres ::",
      width: 800,
      height: 480,
      position: {
        my: "left top",
        at: "left top",
        of: window,
      }
    });
    $("#freeTypePop").dialog({
      autoOpen: false,
      title: "Typographies libres et sauvages ::",
      width: 400,
      height: 700,
      position: {
        my: "right top",
        at: "right top",
        of: window,
      }
    });
    $("#freeToolPop").dialog({
      autoOpen: false,
      title: "Outils conviviaux et libres ::",
      width: 400,
      height: 400,
      position: {
        my: "left bottom",
        at: "left bottom",
        of: window,
      }
    });
    $("#ressourcesPop").dialog({
      autoOpen: false,
      title: "Quelques liens pour aller plus loin ::",
      width: 400,
      height: 400
    });

    $("#todoBtn").click((e) => {
        $("#todoPop").dialog("open");
      });
    $("#valiseBtn").click((e) => {
      $("#valisePop").dialog("open");
    });
    $("#freeImgBtn").click((e) => {
      $("#freeImgPop").dialog("open");
    });
    $("#freeTypeBtn").click((e) => {
      $("#freeTypePop").dialog("open");
    });
    $("#freeToolBtn").click((e) => {
      $("#freeToolPop").dialog("open");
    });
    $("#ressourcesBtn").click((e)=>{
      $("#ressourcesPop").dialog("open");
    });
  });
}); // FIN DOC READY
