function marginItems(item) {
    var i = .1;
    var j = $(window).width() / 1.5;
    item.each(function() {
      if (i < $(window).width() / 1.5) {
        $(this).css({ marginLeft: i + 'px' });
        i = i * 1.2;
        j = $(window).width() / 1.5;
      } else if (j < .1) {
        i = .1;
        $(this).css({ marginLeft: i + 'px' });
        i = i * 1.2;
      } else {
        $(this).css({ marginLeft: j + 'px' });
        j = j / 1.2;
      }
    })
  };



/******* TOGGLE CONTENT ********/
// $("h2").click((event)=>{
//   let target = event.target;
//   target.parentNode.nextElementSibling.classList.toggle("discover");
// });
const trigger = (event) =>{
 let id = event.target.getAttribute("id");
 let target = document.querySelector("."+id);
 event.target.classList.toggle("active");
 target.classList.toggle("discover");
}